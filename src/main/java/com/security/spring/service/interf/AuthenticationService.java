package com.security.spring.service.interf;

import com.security.spring.dto.JwtAuthenticationResponse;
import com.security.spring.dto.RefreshTokenRequest;
import com.security.spring.dto.SignUpRequest;
import com.security.spring.dto.SigninRequest;
import com.security.spring.entities.User;

public interface AuthenticationService {
    User signup(SignUpRequest signUpRequest);
    JwtAuthenticationResponse signin(SigninRequest signinRequest);
    JwtAuthenticationResponse refreshToken(RefreshTokenRequest refreshTokenRequest);
}
