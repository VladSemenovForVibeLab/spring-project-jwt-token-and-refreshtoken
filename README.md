# Spring Boot Spring Security с JWT Token и Refresh Token

Это приложение Spring Boot с применением Spring Security, JWT Token и Refresh Token для авторизации пользователей. Проект также включает использование Swagger для документации API и контейнеризацию с помощью Docker и Docker Compose. В качестве базы данных используется H2 и PostgreSQL с использованием Spring Data JPA и Hibernate.

## Требования

Для запуска и развертывания приложения требуется установка следующих компонентов:

- Java 17
- Docker
- Docker Compose

## Запуск приложения

1. Склонируйте репозиторий на свою локальную машину:

```
git clone https://gitlab.com/VladSemenovForVibeLab/spring-project-jwt-token-and-refreshtoken.git
```

2. Перейдите в директорию проекта:

```
cd spring
```

3. Запустите приложение с помощью Docker Compose:

```
docker-compose up -d
```

Приложение будет запущено и будет доступно по адресу [http://localhost:8080](http://localhost:8080).

## Доступ к API

API может быть использован с помощью Swagger, который доступен по адресу [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html). Здесь вы можете посмотреть доступные эндпойнты, отправлять запросы и получать ответы.

## База данных

Приложение использует H2 для разработки и PostgreSQL для развертывания. При первом запуске приложения, будет использована база данных H2. Приложение автоматически создаст таблицы и заполнит базу данных тестовыми данными.

Для доступа к базе данных H2, вы можете использовать следующие параметры:

- URL: [http://localhost:8080/h2-console](http://localhost:8080/h2-console)
- JDBC URL: jdbc:h2:mem:testdb
- Имя пользователя: sa
- Пароль: пусто

Для доступа к базе данных PostgreSQL, вы можете использовать следующие параметры:

- JDBC URL: jdbc:postgresql://localhost:5432/${}
- Имя пользователя: ${}
- Пароль: ${}

## Конфигурация

Все основные настройки находятся в файле `application.properties`. Здесь вы можете настроить параметры базы данных, порта приложения и другие настройки.