package com.security.spring.entities.enums;

public enum Role {
    USER,
    ADMIN
}
