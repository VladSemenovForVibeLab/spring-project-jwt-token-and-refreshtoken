package com.security.spring.dto;

import lombok.Data;

@Data
public class RefreshTokenRequest {
    private String token;

}
